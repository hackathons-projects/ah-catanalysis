from .FileStorage import component as file_storage
from .RecognitionFactory import component as recognition_factory
from .LectureService import component as lecture_service
