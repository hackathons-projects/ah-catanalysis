import json
import logging

import requests

from domain import AlchemyEncoder
from domain import LectureEntity
from repository import *
from service import recognition_factory

logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


# Service for sending file to model and saving result
class ProcessingService:
    def __init__(self):
        pass

    @staticmethod
    def process(filename, lang, aggressive):
        result = {}
        response = ProcessingService.__recognize(filename, lang, aggressive)
        lecture = LectureEntity(filename, filename, response.text)
        lecture_repository.save(lecture)
        result['text'] = response.text
        result['id'] = lecture.id
        return result

    @staticmethod
    def get_lectures():
        result = []
        for lecture in lecture_repository.get_all():
            result.append(json.loads(json.dumps(lecture, cls=AlchemyEncoder)))

        return json.dumps(result)

    @staticmethod
    def get_lectures_headers():
        result = []
        for lecture in lecture_repository.get_all():
            lecture = json.loads(json.dumps(lecture, cls=AlchemyEncoder))
            lecture['content'] = ''
            result.append(lecture)

        return json.dumps(result)

    @staticmethod
    def get_lecture(id):
        return json.dumps(lecture_repository.get(id), cls=AlchemyEncoder)

    @staticmethod
    def get_lecture_text(id):
        return lecture_repository.get(id).content

    @staticmethod
    def delete_lecture(id):
        lecture_repository.delete(id)

    @staticmethod
    def update_lecture(id, json):
        lecture_repository.update_content(id, json['text'])

    @staticmethod
    def __recognize(filename, lang, aggressive):
        # sending get request and saving the response as response object
        payload = "{\t\n\t\"audio\":  \"" + filename + "\",\n\t\"aggressive\": " + aggressive + "\n\n}"
        headers = {'content-type': "application/json"}

        return requests.request("POST", recognition_factory.get_url(lang),
                                data=payload,
                                headers=headers)


component = ProcessingService()
