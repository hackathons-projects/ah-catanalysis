from domain.Base import session_factory
from domain.LectureEntity import LectureEntity


class LectureRepository:
    def __init__(self):
        self.session = session_factory()

    def __del__(self):
        self.session.close()

    def save(self, lecture):
        self.session.add(lecture)
        self.session.commit()

    def get_all(self):
        return self.session.query(LectureEntity).all()

    def get(self, id):
        return self.session.query(LectureEntity).get(id)

    def delete(self, id):
        entity = self.session.query(LectureEntity).get(id)
        self.session.delete(entity)
        self.session.commit()

    def update_content(self, id, text):
        lecture = self.session.query(LectureEntity).get(id)
        lecture.content = text
        self.session.commit()


component = LectureRepository()
