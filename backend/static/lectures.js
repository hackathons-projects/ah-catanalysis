$(document).ready(function () {
    $.get("/lectures-head").success(function (result) {
        console.log(result)
        for (const lecture of JSON.parse(result)) {
            $("#playlist")[0].innerHTML += '<li id="liId' + lecture['id'] + '">' +
                '<a href="">' + lecture['id'] + '. ' + lecture['filename'] + '</a>' +
                '<a href="'+ EDITOR + lecture['id'] +'"><img src="../static/images/edit.svg" class="download-icon"></a>' +
                '<a href="' + DOWNLOAD_URL + lecture['id'] + '"><img src="../static/images/download-button.svg" class="download-icon"></a>' +
                '<a href="" onclick="deleteLecture(' + lecture['id'] + ')"><img src="../static/images/remove.svg" class="download-icon"></a>' +
                '<p><audio controls="" id="audio" type="audio/wav"><source type="audio/wav" src="/audio/' + lecture['id'] + '"></audio></p>' +
                '</li>'
        }
    });
});

function deleteLecture(id) {
    $.ajax({
        url: LECTURE_URL + id,
        type: 'DELETE',
        success: function (result) {
            console.log("Entity id=" + id + " was deleted")
            $("#liId" + id).remove();
        }
    });
}